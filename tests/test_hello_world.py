"""This is the unit test for the hello_world.py"""
import unittest
import scripts.hello_world as hw


class TestHelloWorld(unittest.TestCase):
    """This is the test class for hello_world.py"""

    def test_main(self):
        """Tests main method of hello_world.py"""
        result = hw.main()
        expected = 'Hello World'
        self.assertEqual(result, expected)
